//
//  CustomerloginViewController.swift
//  wehep2
//
//  Created by Joanna Shi on 2016-01-23.
//  Copyright © 2016 3Jz. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


class CustomerloginViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    
    @IBOutlet weak var fackbookLogin: FBSDKLoginButton!
    @IBOutlet weak var EmailUser: UITextField!
    @IBOutlet weak var PasswordUser: UITextField!
    @IBOutlet weak var Goback: UIButton!
    override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    Goback.titleLabel!.font = UIFont (name: "Times New Roman", size: 20)
    
    if (FBSDKAccessToken.currentAccessToken() == nil)
{ print("not logged in")
    
}else{
    print("logged in")
    }
    
    
    
    fackbookLogin.delegate = self
    fackbookLogin.readPermissions = ["public_profile","email","user_friends"]
    
    }
    
    
    
    override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    
    
    }
    //facebook login in
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
    
    if (error != nil)
{
    print(error.localizedDescription)
    return
    
    }
    if let userToken = result.token
{
 
    let token:FBSDKAccessToken = result.token
    
    print("Token = \(FBSDKAccessToken.currentAccessToken().tokenString)")
    
    print("User ID = \(FBSDKAccessToken.currentAccessToken().userID)")
    
    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUserLoggedIn")
    NSUserDefaults.standardUserDefaults().setValue(result.token.tokenString, forKey: "token")
    NSUserDefaults.standardUserDefaults().synchronize();
    
    
    self.performSegueWithIdentifier("Main2", sender: nil)
    return;
    }
    
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
    print("user is logged out")
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    // Action
    @IBAction func loginbuttom(sender: AnyObject){
    let UserEmail = EmailUser.text
    let UserPassword = PasswordUser.text
    
    let userEmailStored = NSUserDefaults.standardUserDefaults().stringForKey("UserEmail")
    let userPasswordStored = NSUserDefaults.standardUserDefaults().stringForKey("UserPassword")
    
    if(userEmailStored != UserEmail)
{
    //alert
    
    displayAlertMessage("Wrong Username or password");
    return;
    
    }
    
    
    if(userPasswordStored != UserPassword)
{
    
    //alert
    
    displayAlertMessage("Wrong Username or password");
    return;
    }
    
    
    
    //Login is successfull
    
    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isUserLoggedIn")
    NSUserDefaults.standardUserDefaults().synchronize();
    
    
    //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func displayAlertMessage(userMessage:String)
{
    let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.Alert);
    let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.Default, handler:nil)
    
    myAlert.addAction(okAction)
    
    self.presentViewController(myAlert, animated: true, completion: nil)
    
    }
    
}