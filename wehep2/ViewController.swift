//
//  ViewController.swift
//  wehep2
//
//  Created by Joanna Shi on 2016-01-23.
//  Copyright © 2016 3Jz. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(animated: Bool) {
        
          let isUserLoggedIn = NSUserDefaults.standardUserDefaults().boolForKey("isUserLoggedIn");
        if(!isUserLoggedIn)
        {
        self.performSegueWithIdentifier("CandP", sender: self)
    }
    
    }
    
 
    
    
    @IBAction func logoutbutton(sender: AnyObject) {
        
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isUserLoggedIn")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        let LoginManager = FBSDKLoginManager()
        LoginManager.logOut()
        
        
        
        self.performSegueWithIdentifier("CandP", sender: self)
    
    }
    
    
    
}

