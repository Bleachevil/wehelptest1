//
//  CustomerSignupViewController.swift
//  wehep2
//
//  Created by Joanna Shi on 2016-01-23.
//  Copyright © 2016 3Jz. All rights reserved.
//

import UIKit

class CustomerSignupViewController: UIViewController {

    @IBOutlet weak var EmailUser: UITextField!
    @IBOutlet weak var CreatepasswordUser: UITextField!
    @IBOutlet weak var ConfirmPasswordUser: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Action
    
    @IBAction func RegisterUser(sender: AnyObject) {
        
       let username = self.EmailUser.text
        let userpassword = self.CreatepasswordUser.text
        let userCpassword = self.ConfirmPasswordUser.text
        
        //Check for empty fields
        
        if(username!.isEmpty||userpassword!.isEmpty||userCpassword!.isEmpty)
        
        {
            //Display alert message
        
            displayAlertMessage("All fields are required");
            return;
            
        }
        
        
        //Check if passwords match
       if(userpassword != userCpassword)
       {
        
        //Display an alert message
        displayAlertMessage("Passwords do not match"
        );
        return;
        }
        
        
        
        
            // Store data
        
        NSUserDefaults.standardUserDefaults().setObject(username, forKey: "UserEmail")
        NSUserDefaults.standardUserDefaults().setObject(userpassword, forKey: "UserPassword")
        NSUserDefaults.standardUserDefaults().synchronize();
        
        //Display alert message with confirmation
       let myAlert = UIAlertController(title: "Alert", message: "Registration is done.Thank you!", preferredStyle: UIAlertControllerStyle.Alert);
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.Default){ action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        myAlert.addAction(okAction)
        self.presentViewController(myAlert, animated: true, completion: nil)
        
    }
    
    func displayAlertMessage(userMessage:String)
    {
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.Alert);
        let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.Default, handler:nil)
  
    myAlert.addAction(okAction)
    
        self.presentViewController(myAlert, animated: true, completion: nil)
    
    }
    
        
        
        
    }
    

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


